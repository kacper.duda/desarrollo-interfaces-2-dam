﻿namespace WindowsFormsApplicationPizzeria
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBoxSeleccionPizza = new System.Windows.Forms.ComboBox();
            this.labelBienvenido = new System.Windows.Forms.Label();
            this.checkedListBoxIngredientes = new System.Windows.Forms.CheckedListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBoxSeleccionTamaño = new System.Windows.Forms.ComboBox();
            this.comboBoxSeleccionMasa = new System.Windows.Forms.ComboBox();
            this.buttonPedirPizza = new System.Windows.Forms.Button();
            this.richTextBoxResumen = new System.Windows.Forms.RichTextBox();
            this.labelResumen = new System.Windows.Forms.Label();
            this.buttonImporte = new System.Windows.Forms.Button();
            this.buttonVolver1 = new System.Windows.Forms.Button();
            this.labelPorDefecto = new System.Windows.Forms.Label();
            this.checkedListBoxPorDefecto = new System.Windows.Forms.CheckedListBox();
            this.buttonSalir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxSeleccionPizza
            // 
            this.comboBoxSeleccionPizza.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBoxSeleccionPizza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSeleccionPizza.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxSeleccionPizza.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSeleccionPizza.FormattingEnabled = true;
            this.comboBoxSeleccionPizza.IntegralHeight = false;
            this.comboBoxSeleccionPizza.Items.AddRange(new object[] {
            "Selecciona una pizza ...",
            "NewYork (3,5€)",
            "Vegetariana (2€)",
            "Barbacoa Picante (4,5€)",
            "Cuatro Quesos (3€)"});
            this.comboBoxSeleccionPizza.Location = new System.Drawing.Point(351, 143);
            this.comboBoxSeleccionPizza.Name = "comboBoxSeleccionPizza";
            this.comboBoxSeleccionPizza.Size = new System.Drawing.Size(384, 32);
            this.comboBoxSeleccionPizza.TabIndex = 0;
            this.comboBoxSeleccionPizza.SelectedIndexChanged += new System.EventHandler(this.comboBoxSeleccionPizza_SelectedIndexChanged);
            // 
            // labelBienvenido
            // 
            this.labelBienvenido.AutoSize = true;
            this.labelBienvenido.BackColor = System.Drawing.Color.Transparent;
            this.labelBienvenido.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBienvenido.ForeColor = System.Drawing.Color.Black;
            this.labelBienvenido.Location = new System.Drawing.Point(12, 9);
            this.labelBienvenido.Name = "labelBienvenido";
            this.labelBienvenido.Size = new System.Drawing.Size(739, 91);
            this.labelBienvenido.TabIndex = 2;
            this.labelBienvenido.Text = "B I E N V E N I D O";
            // 
            // checkedListBoxIngredientes
            // 
            this.checkedListBoxIngredientes.Enabled = false;
            this.checkedListBoxIngredientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBoxIngredientes.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkedListBoxIngredientes.FormattingEnabled = true;
            this.checkedListBoxIngredientes.Location = new System.Drawing.Point(351, 208);
            this.checkedListBoxIngredientes.Name = "checkedListBoxIngredientes";
            this.checkedListBoxIngredientes.Size = new System.Drawing.Size(384, 244);
            this.checkedListBoxIngredientes.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 143);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(311, 309);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // comboBoxSeleccionTamaño
            // 
            this.comboBoxSeleccionTamaño.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBoxSeleccionTamaño.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSeleccionTamaño.Enabled = false;
            this.comboBoxSeleccionTamaño.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxSeleccionTamaño.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSeleccionTamaño.FormattingEnabled = true;
            this.comboBoxSeleccionTamaño.IntegralHeight = false;
            this.comboBoxSeleccionTamaño.Items.AddRange(new object[] {
            "Selecciona un tamaño ...",
            "Pequeño (3€)",
            "Mediano (4,5€)",
            "Familiar (6,5€)"});
            this.comboBoxSeleccionTamaño.Location = new System.Drawing.Point(351, 467);
            this.comboBoxSeleccionTamaño.Name = "comboBoxSeleccionTamaño";
            this.comboBoxSeleccionTamaño.Size = new System.Drawing.Size(384, 32);
            this.comboBoxSeleccionTamaño.TabIndex = 4;
            // 
            // comboBoxSeleccionMasa
            // 
            this.comboBoxSeleccionMasa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBoxSeleccionMasa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSeleccionMasa.Enabled = false;
            this.comboBoxSeleccionMasa.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxSeleccionMasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSeleccionMasa.FormattingEnabled = true;
            this.comboBoxSeleccionMasa.IntegralHeight = false;
            this.comboBoxSeleccionMasa.Items.AddRange(new object[] {
            "Selecciona un masa ...",
            "Fina (0,5€)",
            "Pan (1€)",
            "Tradicional (1,5€)",
            "Bordes Rellenos (3€)"});
            this.comboBoxSeleccionMasa.Location = new System.Drawing.Point(351, 514);
            this.comboBoxSeleccionMasa.Name = "comboBoxSeleccionMasa";
            this.comboBoxSeleccionMasa.Size = new System.Drawing.Size(384, 32);
            this.comboBoxSeleccionMasa.TabIndex = 5;
            // 
            // buttonPedirPizza
            // 
            this.buttonPedirPizza.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPedirPizza.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPedirPizza.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPedirPizza.Location = new System.Drawing.Point(407, 564);
            this.buttonPedirPizza.Name = "buttonPedirPizza";
            this.buttonPedirPizza.Size = new System.Drawing.Size(241, 55);
            this.buttonPedirPizza.TabIndex = 6;
            this.buttonPedirPizza.Text = "Pedir Pizza";
            this.buttonPedirPizza.UseVisualStyleBackColor = true;
            this.buttonPedirPizza.Click += new System.EventHandler(this.buttonPedirPizza_Click);
            // 
            // richTextBoxResumen
            // 
            this.richTextBoxResumen.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxResumen.Location = new System.Drawing.Point(351, 186);
            this.richTextBoxResumen.Name = "richTextBoxResumen";
            this.richTextBoxResumen.ReadOnly = true;
            this.richTextBoxResumen.Size = new System.Drawing.Size(581, 494);
            this.richTextBoxResumen.TabIndex = 7;
            this.richTextBoxResumen.Text = "";
            this.richTextBoxResumen.Visible = false;
            // 
            // labelResumen
            // 
            this.labelResumen.AutoSize = true;
            this.labelResumen.BackColor = System.Drawing.Color.Transparent;
            this.labelResumen.Font = new System.Drawing.Font("Impact", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResumen.Location = new System.Drawing.Point(387, 33);
            this.labelResumen.Name = "labelResumen";
            this.labelResumen.Size = new System.Drawing.Size(521, 117);
            this.labelResumen.TabIndex = 8;
            this.labelResumen.Text = "R E S U M E N";
            this.labelResumen.Visible = false;
            // 
            // buttonImporte
            // 
            this.buttonImporte.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonImporte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonImporte.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImporte.Location = new System.Drawing.Point(957, 331);
            this.buttonImporte.Name = "buttonImporte";
            this.buttonImporte.Size = new System.Drawing.Size(241, 55);
            this.buttonImporte.TabIndex = 9;
            this.buttonImporte.Text = "IMPORTE";
            this.buttonImporte.UseVisualStyleBackColor = true;
            this.buttonImporte.Visible = false;
            this.buttonImporte.Click += new System.EventHandler(this.buttonImporte_Click);
            // 
            // buttonVolver1
            // 
            this.buttonVolver1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonVolver1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVolver1.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVolver1.Location = new System.Drawing.Point(957, 246);
            this.buttonVolver1.Name = "buttonVolver1";
            this.buttonVolver1.Size = new System.Drawing.Size(241, 55);
            this.buttonVolver1.TabIndex = 10;
            this.buttonVolver1.Text = "VOLVER";
            this.buttonVolver1.UseVisualStyleBackColor = true;
            this.buttonVolver1.Visible = false;
            this.buttonVolver1.Click += new System.EventHandler(this.buttonVolver1_Click);
            // 
            // labelPorDefecto
            // 
            this.labelPorDefecto.AutoSize = true;
            this.labelPorDefecto.BackColor = System.Drawing.Color.Transparent;
            this.labelPorDefecto.Font = new System.Drawing.Font("Impact", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPorDefecto.Location = new System.Drawing.Point(758, 169);
            this.labelPorDefecto.Name = "labelPorDefecto";
            this.labelPorDefecto.Size = new System.Drawing.Size(347, 36);
            this.labelPorDefecto.TabIndex = 11;
            this.labelPorDefecto.Text = "Ingredientes por defecto: ";
            // 
            // checkedListBoxPorDefecto
            // 
            this.checkedListBoxPorDefecto.Enabled = false;
            this.checkedListBoxPorDefecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBoxPorDefecto.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkedListBoxPorDefecto.FormattingEnabled = true;
            this.checkedListBoxPorDefecto.Location = new System.Drawing.Point(764, 208);
            this.checkedListBoxPorDefecto.Name = "checkedListBoxPorDefecto";
            this.checkedListBoxPorDefecto.Size = new System.Drawing.Size(384, 100);
            this.checkedListBoxPorDefecto.TabIndex = 12;
            // 
            // buttonSalir
            // 
            this.buttonSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSalir.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSalir.Location = new System.Drawing.Point(972, 33);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(241, 55);
            this.buttonSalir.TabIndex = 13;
            this.buttonSalir.Text = "SALIR";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = global::WindowsFormsApplicationPizzeria.Properties.Resources.mantel1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1234, 720);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.checkedListBoxPorDefecto);
            this.Controls.Add(this.labelPorDefecto);
            this.Controls.Add(this.buttonVolver1);
            this.Controls.Add(this.buttonImporte);
            this.Controls.Add(this.labelResumen);
            this.Controls.Add(this.buttonPedirPizza);
            this.Controls.Add(this.comboBoxSeleccionMasa);
            this.Controls.Add(this.comboBoxSeleccionTamaño);
            this.Controls.Add(this.checkedListBoxIngredientes);
            this.Controls.Add(this.labelBienvenido);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.comboBoxSeleccionPizza);
            this.Controls.Add(this.richTextBoxResumen);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1250, 759);
            this.MinimumSize = new System.Drawing.Size(1250, 759);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "APLICACION PIZZERIA";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSeleccionPizza;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelBienvenido;
        private System.Windows.Forms.CheckedListBox checkedListBoxIngredientes;
        private System.Windows.Forms.ComboBox comboBoxSeleccionTamaño;
        private System.Windows.Forms.ComboBox comboBoxSeleccionMasa;
        private System.Windows.Forms.Button buttonPedirPizza;
        private System.Windows.Forms.RichTextBox richTextBoxResumen;
        private System.Windows.Forms.Label labelResumen;
        private System.Windows.Forms.Button buttonImporte;
        private System.Windows.Forms.Button buttonVolver1;
        private System.Windows.Forms.Label labelPorDefecto;
        private System.Windows.Forms.CheckedListBox checkedListBoxPorDefecto;
        private System.Windows.Forms.Button buttonSalir;
    }
}

