﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplicationPizzeria
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            comboBoxSeleccionPizza.SelectedIndex = 0;
            comboBoxSeleccionTamaño.SelectedIndex = 0;
            comboBoxSeleccionMasa.SelectedIndex = 0;
        }

        private void InicializaOpciones()
        {
            checkedListBoxIngredientes.Enabled = true;
            comboBoxSeleccionMasa.Enabled = true;
            comboBoxSeleccionTamaño.Enabled = true;
            labelPorDefecto.Enabled = true;
            richTextBoxResumen.Visible = false;
            labelResumen.Visible = false;
            buttonImporte.Visible = false;

            foreach (int i in checkedListBoxIngredientes.CheckedIndices)
            {
                checkedListBoxPorDefecto.Items.Clear();
                checkedListBoxIngredientes.Items.Clear();

                checkedListBoxIngredientes.Enabled = true;
                comboBoxSeleccionMasa.Enabled = true;

                comboBoxSeleccionMasa.SelectedIndex = 0;
                comboBoxSeleccionTamaño.SelectedIndex = 0;
            }

            if (comboBoxSeleccionPizza.SelectedIndex == 0)
            {
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxPorDefecto.Items.Clear();

                buttonPedirPizza.Visible = true;
                checkedListBoxPorDefecto.Visible = false;
                labelPorDefecto.Visible = false;
                checkedListBoxIngredientes.Enabled = false;
                comboBoxSeleccionMasa.Enabled = false;
                comboBoxSeleccionTamaño.Enabled = false;
            }
            if (comboBoxSeleccionPizza.SelectedIndex == 1)
            {
                checkedListBoxPorDefecto.Items.Clear();
                checkedListBoxPorDefecto.Items.AddRange(new object[] {
                   "Jamón York",
                   "Champiñones"
                });
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                    "Calabacín (1€)",
                    "Aceitunas (1€)",
                    "Tabasco (1€)",
                    "Ternera (1€)",
                    "Queso Mozzarella (1€)",
                    "Queso Azul (1€)",
                    "Queso Provolone (1€)",
                    "Queso Parmesano (1€)",
                    "Queso de Cabra (1€)",
                    "Queso Gouda (1€)",
                    "Queso Roquefort (1€)",
                    "Orégano (1€)",
                    "Pepperoni (1€)",
                    "Pollo (1€)",
                    "Cebolla (1€)",
                    "Bacon (1€)",
                    "Carne Picada (1€)",
                    "Tomate (1€)",
                    "Albahaca (1€)",
                    "Pimienta (1€)",
                    "Setas (1€)"
                });
                buttonPedirPizza.Visible = true;
                labelPorDefecto.Visible = true;
                checkedListBoxPorDefecto.Visible = true;
                checkedListBoxIngredientes.Enabled = true;

                checkedListBoxPorDefecto.SetItemChecked(0, true);
                checkedListBoxPorDefecto.SetItemChecked(1, true);

                comboBoxSeleccionTamaño.SelectedIndex = 0;
                comboBoxSeleccionMasa.SelectedIndex = 0;
            }
            if (comboBoxSeleccionPizza.SelectedIndex == 2)
            {
                checkedListBoxPorDefecto.Items.Clear();
                checkedListBoxPorDefecto.Items.AddRange(new object[] {
                   "Calabacín",
                   "Aceitunas"
                });
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                   "Champiñones (1€)",
                   "Orégano (1€)",
                   "Cebolla (1€)",
                   "Tomate (1€)",
                   "Albahaca (1€)",
                   "Pimienta (1€)",
                   "Setas (1€)"
                });
                buttonPedirPizza.Visible = true;
                labelPorDefecto.Visible = true;
                checkedListBoxPorDefecto.Visible = true;
                checkedListBoxPorDefecto.SetItemChecked(0, true);
                checkedListBoxPorDefecto.SetItemChecked(1, true);
            }
            if (comboBoxSeleccionPizza.SelectedIndex == 3)
            {
                checkedListBoxPorDefecto.Items.Clear();
                checkedListBoxPorDefecto.Items.AddRange(new object[] {
                    "Tabasco",
                    "Ternera"
                });
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                    "Jamón York (1€)",
                    "Champiñones (1€)",
                    "Calabacín (1€)",
                    "Aceitunas (1€)",
                    "Queso Mozzarella (1€)",
                    "Queso Azul (1€)",
                    "Queso Provolone (1€)",
                    "Queso Parmesano (1€)",
                    "Queso de Cabra (1€)",
                    "Queso Gouda (1€)",
                    "Queso Roquefort (1€)",
                    "Orégano (1€)",
                    "Pepperoni (1€)",
                    "Pollo (1€)",
                    "Cebolla (1€)",
                    "Bacon (1€)",
                    "Carne Picada (1€)",
                    "Tomate (1€)",
                    "Albahaca (1€)",
                    "Pimienta (1€)",
                    "Setas (1€)"
                });
                buttonPedirPizza.Visible = true;
                checkedListBoxPorDefecto.Visible = true;
                labelPorDefecto.Visible = true;

                comboBoxSeleccionTamaño.SelectedIndex = 0;
                comboBoxSeleccionMasa.SelectedIndex = 0;

                checkedListBoxPorDefecto.SetItemChecked(0, true);
                checkedListBoxPorDefecto.SetItemChecked(1, true);
            }
            if (comboBoxSeleccionPizza.SelectedIndex == 4)
            {
                checkedListBoxPorDefecto.Items.Clear();
                checkedListBoxIngredientes.Items.Clear();
                checkedListBoxIngredientes.Items.AddRange(new object[] {
                  "Queso Mozzarella",
                  "Queso Azul",
                  "Queso Provolone",
                  "Queso Parmesano"
                });
                buttonPedirPizza.Visible = true;
                checkedListBoxPorDefecto.Visible = false;
                labelPorDefecto.Visible = false;
                checkedListBoxIngredientes.Enabled = false;

                comboBoxSeleccionMasa.Enabled = false;
                comboBoxSeleccionTamaño.Enabled = true;
                comboBoxSeleccionTamaño.SelectedIndex = 0;
                comboBoxSeleccionMasa.SelectedIndex = 1;

                checkedListBoxIngredientes.SetItemChecked(0, true);
                checkedListBoxIngredientes.SetItemChecked(1, true);
                checkedListBoxIngredientes.SetItemChecked(2, true);
                checkedListBoxIngredientes.SetItemChecked(3, true);
            }
        }

        public void CompruebaSeleccionComponentes()
        {
            bool encontrado = false;

            for (int i = 0; (i < checkedListBoxIngredientes.Items.Count) && (encontrado == false); i++)
            {
                if (checkedListBoxIngredientes.GetItemChecked(i) || (checkedListBoxIngredientes.CheckedItems.Count == 0))
                {
                    if ((checkedListBoxIngredientes.CheckedItems.Count >= 2) && (checkedListBoxIngredientes.CheckedItems.Count <= 4))
                    {
                        if (comboBoxSeleccionPizza.SelectedIndex == 4)
                        {
                            SacarResumenCuatroQuesos();
                            comboBoxSeleccionPizza.Visible = false;
                            checkedListBoxIngredientes.Visible = false;
                            comboBoxSeleccionTamaño.Visible = false;
                            comboBoxSeleccionMasa.Visible = false;
                            labelBienvenido.Visible = false;
                            labelPorDefecto.Visible = false;
                            buttonPedirPizza.Visible = false;
                            checkedListBoxPorDefecto.Visible = false;
                            richTextBoxResumen.Visible = true;
                            labelResumen.Visible = true;
                            buttonImporte.Visible = true;
                            buttonVolver1.Visible = true;
                            encontrado = true;
                        }
                        else if ((comboBoxSeleccionPizza.SelectedIndex > 0) && (comboBoxSeleccionMasa.SelectedIndex > 0) && (comboBoxSeleccionTamaño.SelectedIndex > 0))
                        {
                            SacarResumen();
                            comboBoxSeleccionPizza.Visible = false;
                            checkedListBoxIngredientes.Visible = false;
                            comboBoxSeleccionTamaño.Visible = false;
                            comboBoxSeleccionMasa.Visible = false;
                            labelBienvenido.Visible = false;
                            labelPorDefecto.Visible = false;
                            buttonPedirPizza.Visible = false;
                            checkedListBoxPorDefecto.Visible = false;
                            richTextBoxResumen.Visible = true;
                            labelResumen.Visible = true;
                            buttonImporte.Visible = true;
                            buttonVolver1.Visible = true;
                            encontrado = true;
                        }
                    }
                    else if((checkedListBoxIngredientes.CheckedItems.Count == 0))
                    {
                        if (comboBoxSeleccionPizza.SelectedIndex == 4)
                        {
                            SacarResumenCuatroQuesos();
                            comboBoxSeleccionPizza.Visible = false;
                            checkedListBoxIngredientes.Visible = false;
                            comboBoxSeleccionTamaño.Visible = false;
                            comboBoxSeleccionMasa.Visible = false;
                            labelBienvenido.Visible = false;
                            labelPorDefecto.Visible = false;
                            buttonPedirPizza.Visible = false;
                            checkedListBoxPorDefecto.Visible = false;
                            richTextBoxResumen.Visible = true;
                            labelResumen.Visible = true;
                            buttonImporte.Visible = true;
                            buttonVolver1.Visible = true;
                            encontrado = true;
                        }
                        else if ((comboBoxSeleccionPizza.SelectedIndex > 0) && (comboBoxSeleccionMasa.SelectedIndex > 0) && (comboBoxSeleccionTamaño.SelectedIndex > 0))
                        {
                            SacarResumen();
                            comboBoxSeleccionPizza.Visible = false;
                            checkedListBoxIngredientes.Visible = false;
                            comboBoxSeleccionTamaño.Visible = false;
                            comboBoxSeleccionMasa.Visible = false;
                            labelBienvenido.Visible = false;
                            labelPorDefecto.Visible = false;
                            buttonPedirPizza.Visible = false;
                            checkedListBoxPorDefecto.Visible = false;
                            richTextBoxResumen.Visible = true;
                            labelResumen.Visible = true;
                            buttonImporte.Visible = true;
                            buttonVolver1.Visible = true;
                            encontrado = true;
                        }
                    }
                    else
                    {
                        string mensage = "Si quieres añadir ingredientes debes seleccionar mínimo 2 y máximo 4 ingredientes extra.";
                        string titulo = "Ingredientes Extra";
                        MessageBoxButtons opciones = MessageBoxButtons.OK;
                        DialogResult result = MessageBox.Show(mensage, titulo, opciones,
                        MessageBoxIcon.Error);
                        encontrado = true;
                    }
                }
            }
            if (encontrado == false)
            {
                string mensage = "Debes seleccionar el tipo de pizza que desea para continuar con el pedido.";
                string titulo = "Campos no seleccionados";
                MessageBoxButtons opciones = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(mensage, titulo, opciones,
                MessageBoxIcon.Error);
            }
        }

        public void SacarResumen ()
        {
            string[] ingredientes_extras = new string[checkedListBoxIngredientes.Items.Count];
            string[] ingredientes_predet = new string[checkedListBoxPorDefecto.Items.Count];

            richTextBoxResumen.Text = "\n PEDIDO SOLICITADO: \n\t - PIZZA: \n\t\t · "
                + comboBoxSeleccionPizza.SelectedItem + "\n\n\t - INGREDIENTES PRINCIPALES: \n\t\t";

            for (int i = 0; i < checkedListBoxPorDefecto.Items.Count; i++)
            {
                if (checkedListBoxPorDefecto.GetItemChecked(i))
                {
                    ingredientes_predet[i] = checkedListBoxPorDefecto.Items[i].ToString();
                    richTextBoxResumen.Text += " · " + ingredientes_predet[i] + "\n\t\t";
                }
            }

            richTextBoxResumen.Text += "\n\t - INGREDIENTES EXTRA: \n\t\t";

            for (int i = 0; i < checkedListBoxIngredientes.Items.Count; i++)
            {
                if (checkedListBoxIngredientes.GetItemChecked(i))
                {
                    ingredientes_extras[i] = checkedListBoxIngredientes.Items[i].ToString();
                    richTextBoxResumen.Text += " · " + ingredientes_extras[i] + "\n\t\t";
                }
            }
            richTextBoxResumen.Text += "\n\t - TAMAÑO: \n\t\t · " + comboBoxSeleccionTamaño.SelectedItem + "\n\n\t - MASA: \n\t\t · " + comboBoxSeleccionMasa.SelectedItem + "\n\n";
        }
        public void SacarResumenCuatroQuesos()
        {
            string[] ingredientes_extras = new string[checkedListBoxIngredientes.Items.Count];

            richTextBoxResumen.Text = "\n PEDIDO SOLICITADO: \n\t - PIZZA: \n\t\t · "
                + comboBoxSeleccionPizza.SelectedItem + "\n\n\t - INGREDIENTES PRINCIPALES: \n\t\t";

            for (int i = 0; i < checkedListBoxIngredientes.Items.Count; i++)
            {
                if (checkedListBoxIngredientes.GetItemChecked(i))
                {
                    ingredientes_extras[i] = checkedListBoxIngredientes.Items[i].ToString();
                    richTextBoxResumen.Text += " · " + ingredientes_extras[i] + "\n\t\t";
                }
            }

            richTextBoxResumen.Text += "\n\t - TAMAÑO: \n\t\t · " + comboBoxSeleccionTamaño.SelectedItem + "\n\n\t - MASA: \n\t\t · " + comboBoxSeleccionMasa.SelectedItem + "\n\n";
        }

        public void AlternarInterfaz()
        {
            comboBoxSeleccionPizza.Visible = true;
            checkedListBoxIngredientes.Visible = true;
            comboBoxSeleccionTamaño.Visible = true;
            comboBoxSeleccionMasa.Visible = true;
            labelBienvenido.Visible = true;
            labelPorDefecto.Visible = true;
            buttonPedirPizza.Visible = true;
            checkedListBoxPorDefecto.Visible = true;
            richTextBoxResumen.Visible = false;
            labelResumen.Visible = false;
            buttonImporte.Visible = false;
            buttonVolver1.Visible = false;
            richTextBoxResumen.Text = "";
        }

        public void MostrarImporte()
        {
            double resultadoPrecio = 0;

            double precioPizza = 0;
            double precioTamaño = 0;
            double precioMasa = 0;
            double precioIngredientes = 0;

            double pizzaNewYork = 3.5;
            double pizzaVegetariana = 2;
            double pizzaBarbacoaPicante = 4.5;
            double pizzaCuatroQuesos = 3;

            double tamañoPequeño = 3;
            double tamañoMediano = 4.5;
            double tamañoFamiliar = 6.5;

            double masaFina = 0.5;
            double masaPan = 1;
            double masaTradicional = 1.5;
            double masaBordesRellenos = 3;

            // SELECCION PIZZAS
            if (comboBoxSeleccionPizza.SelectedIndex == 1)
            {
                precioPizza = pizzaNewYork;
            }
            if (comboBoxSeleccionPizza.SelectedIndex == 2)
            {
                precioPizza = pizzaVegetariana;
            }
            if (comboBoxSeleccionPizza.SelectedIndex == 3)
            {
                precioPizza = pizzaBarbacoaPicante;
            }
            if (comboBoxSeleccionPizza.SelectedIndex == 4)
            {
                precioPizza = pizzaCuatroQuesos;
            }

            // SELECCION TAMAÑO
            if (comboBoxSeleccionTamaño.SelectedIndex == 1)
            {
                precioTamaño = tamañoPequeño;
            }
            if (comboBoxSeleccionTamaño.SelectedIndex == 2)
            {
                precioTamaño = tamañoMediano;
            }
            if (comboBoxSeleccionTamaño.SelectedIndex == 3)
            {
                precioTamaño = tamañoFamiliar;
            }

            // SELECCION MASA
            if (comboBoxSeleccionMasa.SelectedIndex == 1)
            {
                precioMasa = masaFina;
            }
            if (comboBoxSeleccionMasa.SelectedIndex == 2)
            {
                precioMasa = masaPan;
            }
            if (comboBoxSeleccionMasa.SelectedIndex == 3)
            {
                precioMasa = masaTradicional;
            }
            if (comboBoxSeleccionMasa.SelectedIndex == 4)
            {
                precioMasa = masaBordesRellenos;
            }

            // SELECCION INGREDIENTES
            precioIngredientes = checkedListBoxIngredientes.CheckedItems.Count;
            resultadoPrecio = precioPizza + precioIngredientes + precioTamaño + precioMasa;

            string mensage = "El importe de su pedido es: " + resultadoPrecio + "€ \n\n";
            string titulo = "Factura";
            MessageBoxButtons opciones = MessageBoxButtons.OK;
            DialogResult result = MessageBox.Show(mensage, titulo, opciones);
        }

        private void comboBoxSeleccionPizza_SelectedIndexChanged(object sender, EventArgs e)
        {
            InicializaOpciones();
        }

        private void buttonPedirPizza_Click(object sender, EventArgs e)
        {
            CompruebaSeleccionComponentes();
        }

        private void buttonVolver1_Click(object sender, EventArgs e)
        {
            AlternarInterfaz();
        }

        private void buttonImporte_Click(object sender, EventArgs e)
        {
            MostrarImporte();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            string mensage = "¿Realmente quieres salir?";
            string titulo = "Salir del programa";
            MessageBoxButtons opciones = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(mensage, titulo, opciones,
            MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
